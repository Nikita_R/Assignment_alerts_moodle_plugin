<?php

defined('MOODLE_INTERNAL') || die();
require_once('locallib.php');

class block_assignment_alerts extends block_base {
    public function init() {
        $this->title = get_string('pluginname', 'block_assignment_alerts');
    }

    public function get_content() {
        global $CFG, $PAGE;

      	$PAGE->requires->css('/blocks/assignment_alerts/style.css');

	 if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass();

        if (function_exists('block_assignment_alerts_visible_in_block')) {
            $html = block_assignment_alerts_visible_in_block();
            $this->content->text = $html;
        }

	
	return $this->content;
    }

   


}
