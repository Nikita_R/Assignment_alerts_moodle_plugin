<?php
$string['pluginname'] = 'ALERTS';
$string['colapsibleplus'] = '+';
$string['assignment_alerts:addinstance'] = 'Add a new simple HTML block';
$string['assignment_alerts:myaddinstance'] = 'Add a new simple HTML block to My Moodle page';


$string['block_name'] = 'Assignment Alerts';
$string['showhide'] = 'Show/Hide courses';
$string['showcourse'] = 'Show courses';
$string['hidecourse'] = 'Hide courses';
$string['visible_lable'] = 'Visible courses';
$string['hidden_lable'] = 'Hidden courses';
$string['showhide_page_title'] = 'Show/Hide courses';
$string['none'] = 'NONE';
$string['colapsible_plus'] = '+';


