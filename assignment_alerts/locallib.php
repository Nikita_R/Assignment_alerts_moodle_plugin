<?php


function block_assignment_alerts_visible_in_block() {
    global $DB, $USER, $OUTPUT, $CFG;

    $enroledcourses = enrol_get_my_courses();
    block_assignment_alerts_manage_courses($enroledcourses);
    $coursesorder = $DB->get_record('block_myenrolledcoursesorder', array('userid' => $USER->id));
    $coursesinorder = array();
    if (! empty($coursesorder) && is_string($coursesorder->courseorder)) {
        $coursesinorder = json_decode($coursesorder->courseorder, true);
    }
    $html = '';
    $html .= html_writer::start_tag('ul', array('id' => 'course_list_in_block'));
    if (! empty($coursesinorder)) {
    		$coursesinorderstr = implode(', ', $coursesinorder);
        $courses = $DB->get_records_sql('SELECT id, fullname FROM {course} WHERE id IN (' . $coursesinorderstr . ')');
        
        $count = 0;
        foreach ($coursesinorder as $id) {
	        $total_a = $DB->get_records_sql('SELECT count(id) as a FROM {assign} WHERE course = ' .$id);
        	$viewed_a = $DB->get_records_sql('SELECT count(assign_viewed) as b from (select distinct SUBSTRING(other, 26, 1) as assign_viewed from {logstore_standard_log} where SUBSTRING(eventname, 19, 24)  = \'submission_status_viewed\' and courseid =' .$id.') as r');       	

		   	foreach ($total_a as $total){
		   		foreach ($viewed_a as $viewed){
		   			$not_viewed_count = $total->a - $viewed->b;
					if($not_viewed_count != 0){	
								$count++;	
								$url = new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $id));
								$content = html_writer::start_tag('div', array('class' => 'li_course', 'data-id' => $id));
								$anchor = html_writer::link($url, $courses[$id]->fullname);
								$courseicon = get_string('course');
								$courseicon = $OUTPUT->pix_icon('i/course', $courseicon);
								$colapsible = html_writer::start_tag('span', array('class' => 'colapsible_icon'));
								$colapsible .= get_string('colapsibleplus', 'block_assignment_alerts');
								$colapsible .= html_writer::end_tag('span');
								$content .= "$courseicon";
								$content .= "$anchor";
								$content .= " [$not_viewed_count]";
								$content .= "$colapsible";
								$content .= html_writer::end_tag('div');
								$content .= block_assignment_alerts_course_modules($id);
								$html .= html_writer::tag('li', $content, array('class' => 'course_list_item_in_block'));
					}
				}
			}
				
		}
		if($count == 0){
				$content .= html_writer::start_tag('div');
				$content .= html_writer::start_tag('b');
				$content .= "No New Assignments :)";
				$content .= html_writer::end_tag('b');			
				$content .= html_writer::end_tag('div');			
				$html .= html_writer::tag('li', $content);
		}
    }
    $html .= html_writer::end_tag('ul');
    return $html;
}

function block_assignment_alerts_manage_courses($enroledcourses) {
    global $DB, $USER;
    
    $enroledcourseids = array();
    if(! empty($enroledcourses)) {
        foreach ($enroledcourses as $enroledcourse) {
            $enroledcourseids[] = $enroledcourse->id;
        }
    }
    
	

  
    $courseinorderobj = $DB->get_record('block_myenrolledcoursesorder', array('userid' => $USER->id));
    if(! empty($courseinorderobj)) {
        $courseinorder = json_decode($courseinorderobj->courseorder);
        $diff1 = array_diff($courseinorder, $enroledcourseids);
        $diff2 = array_diff($enroledcourseids, $courseinorder);
        asort($diff2);
        if(! empty($diff1) || ! empty($diff2)) {
            $neworder = new stdClass();
            $neworder->id = $courseinorderobj->id;
            $result = array_diff($courseinorder, $diff1);
            $result = array_merge($result, $diff2);
            $result = array_diff($result, $hiddencourseids);
            $neworder->courseorder = json_encode($result);
            $DB->update_record('block_myenrolledcoursesorder', $neworder);
        }
    } else {
        $neworder = new stdClass();
        $neworder->userid = $USER->id;
        $neworder->courseorder = json_encode($enroledcourseids);
        $DB->insert_record('block_myenrolledcoursesorder', $neworder);
    }
}
function block_assignment_alerts_not_viewed_assignment_count($id){
    global $DB, $CFG, $USER;
	 $assign_count = $DB->get_records_sql('SELECT count(id) FROM {assign} WHERE id not IN (SELECT SUBSTRING(other, 26, 1) AS ExtractString FROM {logstore_standard_log} WHERE action = \'viewed\' and courseid = '.$id.')');

	return $assign_count;
}
function block_assignment_alerts_course_modules($id) {
    global $DB, $CFG, $USER;
	//$courses = $DB->get_records_sql('SELECT id, fullname FROM {course} WHERE id IN (' . $coursesinorderstr . ')');
	 $assign_names = $DB->get_records_sql('SELECT id,name FROM {assign} WHERE id not IN (SELECT SUBSTRING(other, 26, 1) AS ExtractString FROM {logstore_standard_log} WHERE action = \'viewed\')');

    $mod_info = get_fast_modinfo($id);

	
	
    $content = '';
    if(! empty($mod_info)) {
        $content .= html_writer::start_tag('div', array('class' => 'course_modules', 'style' => 'display: none'));
        $content .= html_writer::start_tag('ul', array('class' => 'course_modules_list_in_block'));
        foreach($assign_names as $assign_name) {

        foreach($mod_info->cms as $mod) {
            if(($mod->visible == 1) && ($assign_name->name == $mod->name)) {
                $content .= html_writer::start_tag('li', array());
                $mod_url = '';
                if($CFG->version < 2014051200) {
                	$mod_url = $mod->get_url();
                } else {
                	$mod_url = $mod->url;
                }
  
                $content .= html_writer::link($mod_url, $mod->name);
                $content .= html_writer::end_tag('li');
            }
        }
        }
        $content .= html_writer::end_tag('ul');
        $content .= html_writer::end_tag('div');
    }
    return $content;
}

